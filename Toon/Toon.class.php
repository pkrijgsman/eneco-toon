<?php

namespace Eneco\api;

use Exception;


/**
 * Toon class
 * @author Paul Krijgsman <post@paulkrijgsman.nl>
 */
class Toon
{
    private $username;
    private $password;
    private $status;
    private $sessionData = array();
    private $agreements;
    private $randomKey;

    /**
     * @var string
     */
    private $url_login = 'https://toonopafstand.eneco.nl/toonMobileBackendWeb/client/login';

    /**
     * @var string
     */
    private $url_auth = 'https://toonopafstand.eneco.nl/toonMobileBackendWeb/client/auth/start';

    /**
     * @var string
     */
    private $url_logout = 'https://toonopafstand.eneco.nl/toonMobileBackendWeb/client/auth/logout';

    /**
     * @var string
     */
    private $url_status = 'https://toonopafstand.eneco.nl/toonMobileBackendWeb/client/auth/retrieveToonState';

    /**
     * @var string
     */
    private $url_set_temperature = 'https://toonopafstand.eneco.nl/toonMobileBackendWeb/client/auth/setPoint';

    /**
     * @var string
     */
    private $url_set_thermostat_program = 'https://toonopafstand.eneco.nl/toonMobileBackendWeb/client/auth/schemeState';

    /**
     * @var string
     */
    private $url;

    /**
     * @var array
     */
    private $params = array();

    /**
     * Construct Toon.
     *
     * @param string $username
     * @param string $password
     */
    public function __construct($username, $password)
    {
        $this->setRandomkey();
        $this->setUsername($username);
        $this->setPassword($password);
        $this->login();
        $this->auth();
        $this->status();
    }

    /**
     * Login on service.
     */
    private function login()
    {
        $this->setUrl($this->url_login);
        $this->setParameters(
            array(
                'username' => $this->getUsername(),
                'password' => $this->getPassword()
            )
        );
        $this->setSessiondata($this->callService());
        $this->setAgreements();
    }

    /**
     * Authenticate on service.
     */
    private function auth()
    {
        $this->setUrl($this->url_auth);
        $this->setParameters(
            array(
                'clientId' => $this->getSessiondataByKey('clientId'),
                'clientIdChecksum' => $this->getSessiondataByKey('clientIdChecksum'),
                'agreementId' => $this->getAgreementsbyKey('agreementId'),
                'agreementIdChecksum' => $this->getAgreementsbyKey('agreementIdChecksum'),
                'random' => $this->getRandomKey(),
            )
        );
        $this->callService();
    }

    /**
     * Logout on service.
     */
    private function logout()
    {
        $this->setUrl($this->url_logout);
        $this->setParameters(
            array(
                'clientId' => $this->getSessiondataByKey('clientId'),
                'clientIdChecksum' => $this->getSessiondataByKey('clientIdChecksum'),
                'random' => $this->getRandomKey(),
            )
        );
        $this->callService();
    }

    /**
     * Get current status.
     */
    private function status()
    {
        $this->setUrl($this->url_status);
        $this->setParameters(
            array(
                'clientId' => $this->getSessiondataByKey('clientId'),
                'clientIdChecksum' => $this->getSessiondataByKey('clientIdChecksum'),
                'random' => $this->getRandomKey(),
            )
        );
        $this->setStatus($this->callService());
    }

    /**
     * Set thermostat temperature.
     *
     * @param string $value Temperature in de celcius.
     */
    public function setThermostatTemperature($value)
    {
        $temperature = int($value) * 100;
        $this->setUrl($this->url_set_temperature);
        $this->setParameters(
            array(
                'clientId' => $this->getSessiondataByKey('clientId'),
                'clientIdChecksum' => $this->getSessiondataByKey('clientIdChecksum'),
                'value' => $temperature,
                'random' => $this->getRandomKey(),
            )
        );
        $this->callService();
    }

    /**
     * Set thermostat mode.
     *
     * Comfort, sleep, work etc.
     *
     * @param string $state Set state.
     */
    public function setThermostatState($state)
    {
        $this->setUrl($this->url_set_thermostat_program);
        $this->setParameters(
            array(
                'clientId' => $this->getSessiondataByKey('clientId'),
                'clientIdChecksum' => $this->getSessiondataByKey('clientIdChecksum'),
                'state' => $this->getCurrentThermostatState(),
                'temperatureState' => $state,
                'random' => $this->getRandomKey(),
            )
        );
        $this->callService();
    }

    /**
     * Set status.
     *
     * @param array $status
     */
    private function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * Get status.
     *
     * @return array
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Get status by key.
     *
     * @param string $key
     *
     * @return string|array
     */
    public function getStatusByKey($key)
    {
        if (isset($this->status[$key])) {
            return $this->status[$key];
        }
        return;
    }

    /**
     * Get gas usage.
     *
     * @return array
     */
    public function getGasUsage()
    {
        return $this->getStatusByKey('gasUsage');
    }

    /**
     * Get power usage.
     *
     * @return array
     */
    public function getPowerUsage()
    {
        return $this->getStatusByKey('powerUsage');
    }

    /**
     * Get thermostat modes.
     *
     * @return array
     */
    public function getThermostatModes()
    {
        return $this->getStatusByKey('thermostatStates');
    }

    /**
     * Current thermostat status.
     *
     * @return array
     */
    public function getThermostatInfo()
    {
        return $this->getStatusByKey('thermostatInfo');
    }

    /**
     * Get billing info.
     *
     * @return array
     */
    public function getBillingInfo()
    {
        return $this->getStatusByKey('billingInfo');
    }

    /**
     * Get device status info.
     *
     * @return array
     */
    public function getDeviceStatusInfo()
    {
        return $this->getStatusByKey('deviceStatusInfo');
    }

    /**
     * Get device config info.
     *
     * @return array
     */
    public function getDeviceConfigInfo()
    {
        return $this->getStatusByKey('deviceConfigInfo');
    }

    /**
     * Get benchmark data.
     *
     * @return array
     */
    public function getBenchmarkData()
    {
        return $this->getStatusByKey('benchmarkData');
    }

    /**
     * Current thermostat state.
     *
     * @return string
     */
    public function getCurrentThermostatState()
    {
        $data = $this->getThermostatInfo();
        return isset($data['activeState']) ? $data['activeState'] : null;
    }

    /**
     * Call service.
     *
     * @return array
     *
     * @throws Exception
     */
    private function callService()
    {
        $url = trim(sprintf('%s%s', $this->getUrl(), $this->bindParamsToUrl()), '?');

        try {
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_HTTPGET, true);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

            $response = curl_exec($ch);
        } catch(Exception $e) {
            throw new Exception('Did go something wrong!');
        }
        $utf8 = utf8_encode($response);
        $decoded_data = json_decode($utf8, true);

        curl_close($ch);

        return $decoded_data;
    }

    /**
     * Bind parameters to url.
     *
     * @return string
     */
    private function bindParamsToUrl(){
        return sprintf('?%s', http_build_query($this->getParams()));
    }

    /**
     * Set url.
     *
     * @param string $url
     */
    private function setUrl($url)
    {
        $this->url = $url;
    }

    /**
     * Get url.
     *
     * @return string
     */
    private function getUrl()
    {
        return $this->url;
    }

    /**
     * Set parameters.
     *
     * @param array $params
     */
    private function setParameters(array $params)
    {
        $this->params = $params;
    }

    /**
     * Get parameters.
     *
     * Parameters to make the correct call to service.
     *
     * @return array
     */
    private function getParams()
    {
        return $this->params;
    }

    /**
     * Set username.
     *
     * @param string $name
     */
    private function setUsername($name)
    {
        $this->username = $name;
    }

    /**
     * Get username.
     *
     * @return string
     */
    private function getUsername()
    {
        return $this->username;
    }

    /**
     * Set password.
     *
     * @param string $password
     */
    private function setPassword($password)
    {
        $this->password = $password;
    }

    /**
     * Get password.
     *
     * @return string
     */
    private function getPassword()
    {
        return $this->password;
    }

    /**
     * Set session data.
     *
     * @param array $data
     */
    private function setSessiondata(array $data)
    {
        $this->sessionData = $data;
    }

    /**
     * Get session data.
     *
     * @return array
     */
    private function getSessiondata()
    {
        return $this->sessionData;
    }

    /**
     * Get session data by key.
     *
     * @param string $key
     *
     * @return string
     */
    private function getSessiondataByKey($key)
    {
        if (isset($this->sessionData[$key])) {
            return $this->sessionData[$key];
        }
        return;
    }

    /**
     * Set random unique key.
     */
    private function setRandomKey()
    {
        $this->randomKey = uniqid(null);
    }

    /**
     * Get random unique key.
     *
     * @return string
     */
    private function getRandomKey()
    {
        return $this->randomKey;
    }

    /**
     * Set agreements.
     */
    private function setAgreements()
    {
        $this->agreements = current($this->getSessiondataByKey('agreements'));
    }

    /**
     * Get agreements.
     *
     * @return array
     */
    private function getAgreements()
    {
        return $this->agreements;
    }

    /**
     * Get agreements by key.
     *
     * @param string $key
     *
     * @return string
     */
    private function getAgreementsbyKey($key)
    {
        if (isset($this->agreements[$key])) {
            return $this->agreements[$key];
        }
        return;
    }
}
